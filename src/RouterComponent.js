import React, {Component} from 'react';
import {BrowserRouter as Router, Route} from 'react-router-dom';
import {HomePage, About, Settings} from './containers';

class RouterComponent extends Component {
  render() {
    return (
      <Router>
        <div>
          <Route exact path='/' component={HomePage}/>
          <Route path='/about' component={About}/>
          <Route path='/settings' component={Settings}/>
        </div>
      </Router>
    );
  }
}

export default RouterComponent;