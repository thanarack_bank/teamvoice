import HomePage from './HomePage';
import About from './About';
import Settings from './Settings';

export {HomePage, About, Settings}