import React, {Component} from 'react';
import {
  Row,
  Col,
  Icon,
  Card,
  Button,
  Dropdown,
  Menu
} from 'antd';
import Player from '../components/Player/Player';
import '../style/HomePage.css';
import {TopHeader} from '../components/layout';

const ButtonGroup = Button.Group;

const menu = (
  <Menu>
    <Menu.Item key="0">
      <span>เลือกไฟล์</span>
    </Menu.Item>
    <Menu.Item key="1">
      <span>เปิดจากลิงค์</span>
    </Menu.Item>
    <Menu.Divider />
    <Menu.Item key="2">
      <span>ไฟล์แชร์</span>
    </Menu.Item>
  </Menu>
);

class HomePage extends Component {
  constructor() {
    super();
    this.state = {
      play_id: 0
    }
  }

  render() {
    return (
      <div className='component-home'>
        <TopHeader/>
        <Row gutter={10}>
          <Col span={12}>
            <div className='player-1'>
              <Player title='Player 1'/>
            </div>
          </Col>
          <Col span={12}>
            <div className='player-2'>
              <Player title='Player 2'/>
            </div>
          </Col>
        </Row>
        <Row gutter={10}>
          <Col span={8}>
            <div>
              <Card title='คลังเก็บไฟล์' bordered={false} className='card-list'>
                <div className='button-box'>
                  <ButtonGroup>
                    <Dropdown overlay={menu} trigger={['click']}>
                      <Button icon='plus'/>
                    </Dropdown>
                    <Button icon='minus'/>
                    <Button icon='search'/>
                    <Button icon='reload'/>
                    <Button icon='save'/>
                  </ButtonGroup>
                </div>
                <div className='box-card-list'></div>
              </Card>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <Card title='รายการรอเล่น' bordered={false} className='card-list'>
                <div className='button-box'>
                  <ButtonGroup>
                    <Dropdown overlay={menu} trigger={['click']}>
                      <Button icon='plus'/>
                    </Dropdown>
                    <Button icon='minus'/>
                    <Button icon='search'/>
                    <Button icon='reload'/>
                    <Button icon='save'/>
                  </ButtonGroup>
                </div>
                <div className='box-card-list'></div>
              </Card>
            </div>
          </Col>
          <Col span={8}>
            <div>
              <Card title='ประวัติการเล่น' bordered={false} className='card-list'>
                <div className='button-box'>
                <ButtonGroup>
                    <Button icon='close-circle-o'/>
                  </ButtonGroup>
                </div>
                <div className='box-card-list'></div>
              </Card>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default HomePage;