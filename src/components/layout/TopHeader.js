import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Row, Col, Icon, Switch} from 'antd';
import Time from '../Time/Time';
import '../../style/TopHeader.css'

class TopHeader extends Component {
  render() {
    return (
      <div>
        <Row className='no-mb'>
          <Col span={24}>
            <div className='navigator'>
              <ul className='menu lt-nav'>
                <li>
                  <Switch defaultChecked/>
                  <span className='txt-dissound'>ปิด-เปิดเสียง</span>
                </li>
              </ul>
              <ul className='menu rt-nav'>
                <li><Icon type="safety"/>
                  สถานะ : ปกติ</li>
                <li>
                  <Link to='/settings'><Icon type="setting"/>
                    ตั้งค่าระบบ</Link>
                </li>
                <li><Link to='/about'><Icon type="info-circle-o"/>
                  เกี่ยวกับโปรแกรม</Link></li>
                <li><Icon type="clock-circle-o"/>
                  <Time/>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default TopHeader;