import React, {Component} from 'react';
import {Link} from 'react-router-dom';
import {Row, Col, Icon, Switch} from 'antd';
import Time from '../Time/Time';
import '../../style/TopHeader.css'

class TopHeaderSub extends Component {
  render() {
    return (
      <div>
        <Row className='no-mb'>
          <Col span={24}>
            <div className='navigator'>
              <ul className='menu lt-nav'>
                <li>
                  <Link to='/'><Icon type='left' />
                    ย้อนกลับ</Link>
                </li>
              </ul>
            </div>
          </Col>
        </Row>
      </div>
    );
  }
}

export default TopHeaderSub;