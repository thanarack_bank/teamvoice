import React, {Component} from 'react';
import {Row, Col, Icon, Card, Slider} from 'antd';
import '../../style/Player.css';

class Player extends Component {
  constructor() {
    super();
    this.state = {
      progressPlay: 0,
      totalTime: 0,
      currentTime: 0,
      bitrate: 128,
      soundType: 'สเตอริโอ',
      play: false,
      volume: 70,
      lastVolume: 70
    }
  }

  getProgessValue = (value) => {
    this.setState({progressPlay: value});
  }

  handleMute = () => {
    if (this.state.volume === 0) {
      this.setState({volume: this.state.lastVolume});
    } else {
      this.setState({volume: 0});
    }
  }

  handleSlideVolume = (value) => {
    this.setState({volume: value, lastVolume: value});
  }

  render() {
    const props = this.props;
    return (
      <div className='player-box'>
        <Card bordered={false}>
          <Row>
            <Col span={22}>
              <Row>
                <Col span={2}>
                  <div className='disc-box'>
                    <Icon
                      className={this.state.play
                      ? 'play'
                      : null}
                      type='caret-right'/>
                  </div>
                </Col>
                <Col span={22}>
                  <p className='title-play'>กำลังเล่น - เชือกวิเศษ</p>
                  <p className='desc-play'>ระยะเวลา: 00:00 , เวลาที่เล่น: 00:00 , ความละเอียด: 125k , ระบบเสียงสเตอริโอ</p>
                </Col>
              </Row>
              <Row className='no-mb'>
                <Col span={24}>
                  <div className='player-slider'>
                    <ul className='control-player'>
                      <li><Icon type='pause-circle-o' title='หยุด'/></li>
                      <li><Icon type='backward' title='ย้อนหลัง 5 วินาที'/></li>
                      <li><Icon type='forward' title='ไปข้างหน้า 5 วินาที'/></li>
                      <li><Icon type='logout' title='ค่อยๆจบเพลง'/></li>
                    </ul>
                    <Slider defaultValue={this.state.progressPlay} onChange={this.getProgessValue}/>
                  </div>
                </Col>
              </Row>
            </Col>
            <Col span={2}>
              <div className='volume'>
                <Icon
                  className={this.state.volume === 0
                  ? 'mute'
                  : 'no-mute'}
                  type='sound'
                  onClick={this.handleMute}/>
                <Slider vertical value={this.state.volume} onChange={this.handleSlideVolume}/>
              </div>
            </Col>
          </Row>
        </Card>
      </div>
    );
  }
}

export default Player;