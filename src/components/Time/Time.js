import React, {Component} from 'react';

class Time extends Component {
    constructor() {
        super();
        this.state = {
            time_str: ''
        }
    }

    componentDidMount() {
        this.updateTime();
        this.startTime = setInterval(this.updateTime, 1000);
    }

    componentWillUnmount() {
        clearInterval(this.startTime);
    }

    updateTime = () => {
        const currentTime = new Date()
        const hours = currentTime.getHours()
        let minutes = currentTime.getMinutes()
        if (minutes < 10) {
            minutes = '0' + minutes
        }
        const t_str = hours + ':' + minutes + ' ';
        /* if (hours > 11) {
            t_str += "PM";
        } else {
            t_str += "AM";
        } */
        this.setState({time_str: t_str});
    }

    render() {
        return (
            <span className='blink'>{this.state.time_str}</span>
        );
    }

}

export default Time;