import React, {Component} from 'react';
import 'antd/dist/antd.css';
import './App.css';
import RouterComponent from './RouterComponent';

class App extends Component {
  render() {
    return (
      <div className="App">
        <RouterComponent/>
      </div>
    );
  }
}

export default App;
